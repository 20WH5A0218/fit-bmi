
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RegService } from '../reg.service';


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  register: any;

  constructor(private service: RegService) { 
    this.register = {Id:'',Name:'',dob:'',Gender:'',mail:'',password:''}
  }  
  ngOnInit(): void {
    console.log('data recieved...');
  }
   
/*  regSubmit(): void {
    console.log(this.register);
  }
*/
RegService(){
   this.service.RegService(this.register).subscribe();
 }
}
